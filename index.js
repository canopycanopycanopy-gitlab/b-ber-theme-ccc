const path = require('path')
const npmPackage = require('./package.json')

module.exports = {
  name: '@canopycanopycanopy/b-ber-theme-ccc',
  entry: path.join(__dirname, 'application.scss'),
  fonts: [],
  images: [],
  npmPackage,
}
