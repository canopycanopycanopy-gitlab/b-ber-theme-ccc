# `b-ber-theme-ccc`

A Triple Canopy specific b-ber theme based on [b-ber-theme-serif](https://github.com/triplecanopy/b-ber/tree/master/packages/b-ber-theme-serif)
